import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { HeroesService,Heroe } from '../../services/heroes.service';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.css']
})
export class BusquedaComponent implements OnInit {

  private heroes : Heroe[];

  constructor(private _activatedRoute : ActivatedRoute, private _heroesService : HeroesService,private _router : Router) { }

  ngOnInit() {
    this._activatedRoute.params.subscribe( params => {
      this.heroes = this._heroesService.buscarHeroe( params['heroe']);
    })
  }

  //Funcion para redireccionar via codigo. (Necesario importar 'Router' desde '@angular/router')
  verHeroe(idx:number){
    this._router.navigate(['/heroe',idx]);
    //console.log("El index del heroe es: " + idx)
  }

}
