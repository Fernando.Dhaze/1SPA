import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { HeroesService, Heroe } from '../../services/heroes.service';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styleUrls: ['./heroe.component.css']
})
export class HeroeComponent implements OnInit {

private heroe : Heroe;

  constructor(private _activatedRoute : ActivatedRoute, private  _heroeService : HeroesService, private _router : Router) {
    //Recibir parametros desde otro componente o pagina. (Se requiere importar 'ActvatedRoute' desde '@angular/router')
    this._activatedRoute.params.subscribe( params => {
      this.heroe = this._heroeService.getHeroe(params['id']);
      //console.log(params['id']);
      //console.log(this.heroe);
    })
  }

  ngOnInit() {
  }

  //Funcion para redirigir la pagina a 'Heroes'. (Se requiere importar 'Router' desde '@angular/router')
  irHeroes(){
    this._router.navigate(['/heroes']);
  }

}
