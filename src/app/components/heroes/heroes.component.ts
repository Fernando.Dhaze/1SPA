import { Component, OnInit } from '@angular/core';
import { HeroesService,Heroe } from '../../services/heroes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  private heroes : Heroe[] = [];

  constructor(private _heroesService:HeroesService,private _router:Router) {

  }

  ngOnInit() {
    this.heroes = this._heroesService.getHeroes();
    console.log(this.heroes);
  }

  //Funcion para redireccionar via codigo. (Necesario importar 'Router' desde '@angular/router')
  verHeroe(idx:number){
    this._router.navigate(['/heroe',idx]);
    //console.log("El index del heroe es: " + idx)
  }

}
